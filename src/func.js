const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false
  } else if (/[a-zа-яё]/i.test(str1) || /[a-zа-яё]/i.test(str2)) {
    return false
  } else {
    return String(Number(str1) + Number(str2))
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count = 0;
  let comm = 0;
  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].author === authorName) {
      count++
    }
    if (listOfPosts[i].hasOwnProperty('comments')) {
      let nextLvl = listOfPosts[i].comments
      for (let j = 0; j < nextLvl.length; j++) {
        if (nextLvl[j].author === authorName) {
          comm++
        }
      }
    }
  }
  return `Post:${count},comments:${comm}`
};

const tickets = (people) => {
  let count = 0
  let arr = people.map(el => +el)
  for (let i = 0; i < arr.length; i++) {
    count += 25
    if (arr[i] === 50) {
      count -= 25
    } else if (arr[i] === 100) {
      count -= 75
    }
  }
  if (count > 0) {
    return 'YES'
  } else {
    return 'NO'
  }
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
